/*
 * Copyright (C) 2016  tr808axm
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.tr808axm.launcher;

import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * Represents a launchable.
 * Created by tr808axm on 26.10.2016.
 */
public class Launchable {
    private final String name;
    private final File file;

    public Launchable(String name, File launchableFile) {
        if (name == null || name.isEmpty()) throw new IllegalArgumentException("name may not be empty");
        if (launchableFile == null || !launchableFile.exists() || !launchableFile.canExecute())
            throw new IllegalArgumentException("launchableFile must be an existing file with execute access");
        this.name = name;
        this.file = launchableFile;
    }

    public Launchable(File launchableFile) {
        this(launchableFile.getName(), launchableFile);
    }

    public void launch() {
        System.out.println("Launching " + name);
        try {
            if (Desktop.isDesktopSupported())
                if (Desktop.getDesktop().isSupported(Desktop.Action.OPEN))
                    Desktop.getDesktop().open(file);
                else System.out.println("Could not launch: Action OPEN is not supported!");
            else System.out.println("Could not launch: Desktop not supported!");
        } catch (IOException e) {
            System.out.println("IOException at launching " + name + ": " + e.getClass().getSimpleName() + ": " + e.getMessage());
        }
    }

    public String getName() {
        return name;
    }

    public File getFile() {
        return file;
    }
}
