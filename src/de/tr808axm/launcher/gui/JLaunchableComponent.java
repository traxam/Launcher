/*
 * Copyright (C) 2016  tr808axm
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.tr808axm.launcher.gui;

import de.tr808axm.launcher.Launchable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;

/**
 * Represents a launchable as a GUI component.
 * Created by tr808axm on 22.10.2016.
 */
public class JLaunchableComponent extends JPanel {
    private static Color backgroundColor = new Color(236, 240, 241);
    private final Launchable assignedLaunchable;

    public JLaunchableComponent(Launchable launchable) {
        super();
        this.assignedLaunchable = launchable;
        setPreferredSize(new Dimension(50, 75));
        addMouseListener(new MouseClickListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                assignedLaunchable.launch();
            }
        });
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        setBackground(backgroundColor);
        g.setColor(Color.GREEN);
        int horizontalMarginSize = getHeight() / 10;
        int verticalMarginSize = getWidth() / 10;
        g.fillRect(verticalMarginSize, horizontalMarginSize,
                getWidth() - 2 * verticalMarginSize, getHeight() - 2 * horizontalMarginSize);
        g.setColor(Color.BLACK);
        g.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 9));
        g.drawString(assignedLaunchable.getName(), (int) (getWidth() * 0.05), (int) (getHeight() * 0.95));
    }
}
