/*
 * Copyright (C) 2016  tr808axm
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.tr808axm.launcher;

import de.tr808axm.launcher.gui.LauncherGUI;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * This is the access point for launching the project.
 * Created by tr808axm on 18.10.2016.
 */
public class Launcher {
    private final LauncherGUI gui;
    private final List<Launchable> launchables;
    private static final File DEFAULT_STROAGE_FILE = new File(System.getProperty("user.home") + File.separator
            + ".config" + File.separator
            + "Launcher" + File.separator
            + ".launchables");

    private Launcher() {
        this.gui = new LauncherGUI(this);
        launchables = new ArrayList<>();
        if (!DEFAULT_STROAGE_FILE.exists()) {
            DEFAULT_STROAGE_FILE.getParentFile().mkdirs();
            try {
                DEFAULT_STROAGE_FILE.createNewFile();
            } catch (IOException e) {
                System.out.println("Could not create default storage file: " + e.getClass().getSimpleName() + ": " + e.getMessage());
            }
        }
        loadLaunchables(DEFAULT_STROAGE_FILE);
    }

    public static void main(String[] args) {
        new Launcher();
    }

    public void addLaunchable(Launchable launchable, boolean save) {
        if (launchable == null) throw new NullPointerException("launchable may not be null");
        launchables.add(launchable);
        gui.createLauchableComponent(launchable); //TODO extract this using events
        if (save) saveLaunchables(DEFAULT_STROAGE_FILE);
    }

    private void loadLaunchables(File storageFile) {
        System.out.println("Loading launchables from storage file...");
        launchables.clear();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(storageFile));
            String launchablePath;
            while ((launchablePath = reader.readLine()) != null) {
                File launchableFile = new File(launchablePath);
                addLaunchable(new Launchable(launchableFile), false);
            }
        } catch (IOException e) {
            System.out.println("Could not read launchable storage File: " + e.getClass().getSimpleName() + ": " + e.getMessage());
        } finally {
            if (reader != null) try {
                reader.close();
            } catch (IOException e) { //Suppress
            }
        }
    }

    private void saveLaunchables(File storageFile) {
        System.out.println("Saving launchables to storage file...");
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(storageFile));
            for (Launchable launchable : launchables) {
                writer.append(launchable.getFile().getAbsolutePath());
                writer.newLine();
            }
            writer.flush();
        } catch (IOException e) {
            System.out.println("Could not write launchable storage File: " + e.getClass().getSimpleName() + ": " + e.getMessage());
        } finally {
            if (writer != null) try {
                writer.close();
            } catch (IOException e) { //Suppress
            }
        }
    }
}
